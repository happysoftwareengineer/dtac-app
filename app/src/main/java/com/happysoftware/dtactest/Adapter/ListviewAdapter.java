package com.happysoftware.dtactest.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.happysoftware.dtactest.R;

/**
 * Created by Rawit on 9/26/2017 AD.
 */

public class ListviewAdapter extends BaseAdapter {
    Context mContext;
    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row_layout = (View)LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout,parent,false);


        return row_layout;
    }
}
