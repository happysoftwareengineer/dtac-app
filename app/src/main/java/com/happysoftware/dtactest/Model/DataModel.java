package com.happysoftware.dtactest.Model;

/**
 * Created by Rawit on 9/26/2017 AD.
 */

public class DataModel {

    /**
     * titel : Dtac app
     * date : 02/06
     * message : message
     */

    private String titel;
    private String date;
    private String message;

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
