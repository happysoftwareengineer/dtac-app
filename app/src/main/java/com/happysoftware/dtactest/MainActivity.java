package com.happysoftware.dtactest;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.happysoftware.dtactest.Adapter.ListviewAdapter;

public class MainActivity extends AppCompatActivity {

    ListView mlistView;
    ListviewAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mlistView = (ListView)findViewById(R.id.listView);
        mAdapter = new ListviewAdapter();
        mlistView.setAdapter(mAdapter);

        mlistView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                manageExpandableListView(view,position);
            }
        });

    }
    private void manageExpandableListView(View viewOnClick,int position)
    {
        ViewGroup.LayoutParams lp = viewOnClick.getLayoutParams();
        if(lp.height !=ActionBar.LayoutParams.WRAP_CONTENT)
        {
            lp.height = ActionBar.LayoutParams.WRAP_CONTENT;
        }
        else
        {
            lp.height = 240;
        }
        viewOnClick.setLayoutParams(lp);
        viewOnClick.requestLayout();
    }
}
